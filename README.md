# alta3research-ansible-cert

Certification for Shiju Wilson

## Getting Started

Plabook for certification from Alta3

### Prerequisites

Hosts in the lab.  No Windows VMs used but code is written, but commented for future use

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Shiju Wilson** - *Initial work* - [YourWebsite](https://example.com/)
